package model;

import java.io.Serializable;

@SuppressWarnings("serial")
public class Person implements Serializable{
	
	
	private Long id;
	 private String firstName;
	 private String lastName;
	 private Long phoneName;
	
	
	 public Person(Long id, String firstName, String lastName, Long phoneName) {
		super();
		this.id = id;
		this.firstName = firstName;
		this.lastName = lastName;
		this.phoneName = phoneName;
	}
	 
	@Override
	public String toString() {
		return  id + "////" + firstName + "////" + lastName + "////" + phoneName;
	}

	public Person() {
		super();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}



	public String getFirstName() {
		return firstName;
	}



	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}



	public String getLastName() {
		return lastName;
	}



	public void setLastName(String lastName) {
		this.lastName = lastName;
	}



	public Long getPhoneName() {
		return phoneName;
	}



	public void setPhoneName(Long phoneName) {
		this.phoneName = phoneName;
	}



}
