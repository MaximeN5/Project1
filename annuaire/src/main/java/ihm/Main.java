package ihm;


import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.Scanner;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import dao.ContactDaoImpl;
import model.Person;
import service.ContactServiceImpl;

public class Main {
	
	private static final Logger logger = LoggerFactory.getLogger(Main.class);
	 

	@SuppressWarnings("resource")
	public static void main(String[] args) throws IOException, InterruptedException {
	    String str1,str2,str3,str4;
	    Person p ;
		ApplicationContext context = new ClassPathXmlApplicationContext("servlet-context.xml");
		
		ContactServiceImpl service = context.getBean("ContactService",ContactServiceImpl.class);
		
		File file = new File("src/main/resources/ListContact");
		
		//ContactDaoImpl contact = new ContactDaoImpl();
		//service.setContact(contact);
		System.out.println("Bonjour utilisateur:");
		
		Scanner sc = new Scanner(System.in);
		presentation();
		int param = 0;
		
		String str = sc.nextLine();
		
		while(Integer.valueOf(str)!=5){
			if(param!=0){
				presentation();
				str = sc.nextLine();
			}
			param = 1;
			switch (Integer.valueOf(str))
			{
			  case 0:
				  Long index = getIdDispo(service,file);
				  System.out.println("Rentrer un prenom: ");
				  str1 = sc.nextLine();
				  System.out.println("Rentrer un nom: ");
				  str2 = sc.nextLine();
				  System.out.println("Rentrer un Numero de telephone: ");
				  str3 = sc.nextLine();
				  p = new Person(index,str1,str2,Long.parseLong(str3));
				  service.createPerson(file, p);
			    break;
			  case 1:
				 System.out.println("Rentrer une id");
				 str1 = sc.nextLine();
				 if(service.getPerson(file, Long.parseLong(str1))==null){
					 System.out.println("Le contact demandé n'existe pas \n\n");
				 }
				 else{
					  System.out.println("Rentrer un nouveau prenom:");
					  str2 = sc.nextLine();
					  System.out.println("Rentrer un nouveau nom:");
					  str3 = sc.nextLine();
					  System.out.println("Rentrer un nouveau Numero de telephone:");
					  str4 = sc.nextLine();
					  p = new Person(Long.parseLong(str1),str2,str3,Long.parseLong(str4));
					  service.updatePerson(file, p);
					  System.out.println("Mise à jour effectuer"); 
				 }
			    break;
			  case 2:
				  System.out.println("Rentrer une id");
					 str1 = sc.nextLine();
					 if(service.getPerson(file, Long.parseLong(str1))==null){
						 System.out.println("Le contact demandé n'existe pas \n\n");
					 }
					 else{
						 service.deletePerson(file,Long.parseLong(str1));
						 System.out.println("Le contact a bien été éffacé \n\n");
					 }
			    break;
			  case 3:
				  System.out.println("Rentrer une id");
				  str1 = sc.nextLine();
					 if(service.getPerson(file, Long.parseLong(str1))==null){
						 System.out.println("Le contact demandé n'existe pas \n\n");
					 }
					 else{
						 System.out.println(service.getPerson(file, Long.parseLong(str1)).toString()+"\n\n");
					 }
				break;
			  case 4:
				   Collection<Person> listePerson = service.listePerson(file);
					for(Person pe: listePerson){
						System.out.println(pe.toString());
					}
					break;
			  case 5:
				System.out.println("Merci d'avoir utiliser mon programme !");
			    break;
			  default:
			    System.out.println("Commande inconnue!!");
			}
		}
	}
	
	
	static void presentation() throws InterruptedException{
		int time = 1000;
		System.out.println("\n\n");
		System.out.println("Pour creer un contact taper 0");
		Thread.sleep(time);
		System.out.println("Pour mettre à jour un contact taper 1");
		Thread.sleep(time);
		System.out.println("Pour effacer un contact taper 2");
		Thread.sleep(time);
		System.out.println("Pour Obtenir les informations sur un contact taper 3");
		Thread.sleep(time);
		System.out.println("Pour Obtenir la liste de tous les contacts taper 4");
		Thread.sleep(time);
		System.out.println("Pour sortir taper 5");
		System.out.println("\n\n\n\n");
	}
	
	
	static Long getIdDispo(ContactServiceImpl service, File file){
		Long index = 1L;
		Long indexR = 1L;
		Collection<Person> listePerson = service.listePerson(file);	
		do{	
			index = indexR;
				for(Person p: listePerson){
					if(p.getId().equals(index)){
						indexR++;
					}
				}
		}while(index!=indexR);
	return index;	
	}

}
