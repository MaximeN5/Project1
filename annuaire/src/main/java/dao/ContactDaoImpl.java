package dao;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.Writer;
import java.nio.channels.FileChannel;
import java.nio.file.Files;
import java.util.ArrayList;
import java.util.Collection;

import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Repository;

import static java.nio.file.StandardCopyOption.*;

import model.Person;


@Repository
public class ContactDaoImpl implements ContactDao{
	

	public ContactDaoImpl() {
		super();
	}
	

   public Person get(File f , Long id) {
		Person person = null;
		try{
				InputStream flux = new FileInputStream(f); 
				InputStreamReader lecture = new InputStreamReader(flux);
				BufferedReader buff = new BufferedReader(lecture);
				String line;
					while ((line=buff.readLine())!=null){
						String delims = "////";
						String[] tokens = line.split(delims);
						if(Long.parseLong(tokens[0])==id){
							person = new Person(Long.parseLong(tokens[0]),tokens[1],tokens[2],Long.parseLong(tokens[3]));
						}
					}
				buff.close(); 
			}		
			catch (Exception e){
			System.out.println(e.toString());
			}
		return person;	
	}
	
	public void deletePerson(File f , Long id) throws IOException{
		Writer output = null;
		File file= new File("src/main/resources/ouput");
		output = new BufferedWriter(new FileWriter("src/main/resources/ouput"));
		String newline = System.getProperty("line.separator");
		         
		FileInputStream in = new FileInputStream(f);
		BufferedReader br = new BufferedReader(new InputStreamReader(in));
		String line;
		                 
		while ((line = br.readLine()) != null) {
			String delims = "////";
			String[] tokens = line.split(delims);
			if(Long.parseLong(tokens[0])==id){
				System.out.println("Line Deleted.");
				System.out.println("");
			}else{
				// Write non deleted lines to file
				output.write(line);
				output.write(newline);
				}
			    
		}
		in.close();
		output.close(); 
		copyFileUsingStream(file, f);
}
		
	
	

	private static void copyFileUsingStream(File source, File dest) throws IOException {
	    InputStream is = null;
	    OutputStream os = null;
	    try {
	        is = new FileInputStream(source);
	        os = new FileOutputStream(dest);
	        byte[] buffer = new byte[1024];
	        int length;
	        while ((length = is.read(buffer)) > 0) {
	            os.write(buffer, 0, length);
	        }
	    } finally {
	        is.close();
	        os.close();
	    }
	}
	

		public void createPerson(File f, Person p) {
			try
			{
			    FileWriter fw = new FileWriter (f,true);
			        fw.write (p.toString());
			        fw.write ("\n"); 
			    fw.close();
			}
			catch (IOException exception)
			{
			    System.out.println ( "Erreur lors de la lecture : " + exception.getMessage());
			}
		}

		public void updatePerson(File f, Person p) throws IOException {
			Writer output = null;
			File file= new File("src/main/resources/ouput");
			output = new BufferedWriter(new FileWriter("src/main/resources/ouput"));
			String newline = System.getProperty("line.separator");
			         
			FileInputStream in = new FileInputStream(f);
			BufferedReader br = new BufferedReader(new InputStreamReader(in));
			String line;
			                 
			while ((line = br.readLine()) != null) {
				String delims = "////";
				String[] tokens = line.split(delims);
				if(Long.parseLong(tokens[0])==p.getId()){
					output.write(p.toString());
					output.write(newline);
				}else{
					output.write(line);
					output.write(newline);
					}    
			}
			in.close();
			output.close(); 
			copyFileUsingStream(file, f);
		}

		public Person getPerson(File f, Long id) {
			Person person = null;
			try{
					InputStream flux=new FileInputStream(f); 
					InputStreamReader lecture=new InputStreamReader(flux);
					BufferedReader buff=new BufferedReader(lecture);
					String line;
						while ((line=buff.readLine())!=null){
							String delims = "////";
							String[] tokens = line.split(delims);
							if(Long.parseLong(tokens[0])==id){
								person = new Person(Long.parseLong(tokens[0]),tokens[1],tokens[2],Long.parseLong(tokens[3]));
							}
						}
					buff.close(); 
				}		
				catch (Exception e){
				System.out.println(e.toString());
				}
			return person;	
		}

		public Collection<Person> listePerson(File f) {
			   Collection<Person> Cp = new ArrayList<Person>();
			   try{
					InputStream flux=new FileInputStream(f); 
					InputStreamReader lecture=new InputStreamReader(flux);
					BufferedReader buff=new BufferedReader(lecture);
					String line;
						while ((line=buff.readLine())!=null){
							String delims = "////";
							String[] tokens = line.split(delims);
							Person person = null;
						    person = new Person(Long.parseLong(tokens[0]),tokens[1],tokens[2],Long.parseLong(tokens[3]));
							Cp.add(person);
						}
					buff.close(); 
				}		
				catch (Exception e){
				System.out.println(e.toString());
				}
			return Cp;	
		}




}
