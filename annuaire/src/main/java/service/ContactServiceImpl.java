package service;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Service;

import dao.ContactDao;
import model.Person;

@Service
public class ContactServiceImpl implements ContactService{
	
	@Autowired
	private ContactDao contact;

	public Person getPerson(File f, Long id) {
		return contact.getPerson(f,id);
	}

	public void createPerson(File f, Person p) {
		contact.createPerson(f,p);
	}

	public void updatePerson(File f, Person p) throws IOException {
		contact.updatePerson(f, p);
	}

	public Collection<Person> listePerson(File f) {
		return contact.listePerson(f);
	}

	public ContactDao getContact() {
		return contact;
	}

	public void setContact(ContactDao contact) {
		this.contact = contact;
	}

	public void deletePerson(File f, Long id) throws IOException {
		contact.deletePerson(f, id);
		
	}
	



}

	




