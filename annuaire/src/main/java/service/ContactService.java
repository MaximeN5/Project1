package service;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import model.Person;

public interface ContactService {
	
	public void createPerson(File f ,Person p);
	 
	public void updatePerson(File f ,Person p) throws IOException;
	
	public void deletePerson(File f ,Long id) throws IOException;
	 
	public Person getPerson(File f ,Long id);
	 
	public Collection<Person> listePerson(File f);

}
