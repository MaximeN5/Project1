package Test;

import static org.junit.Assert.*;

import java.io.File;
import java.io.IOException;
import java.util.Collection;

import org.junit.Before;
import org.junit.Test;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import model.Person;
import service.ContactServiceImpl;

@TestExecutionListeners( { DependencyInjectionTestExecutionListener.class })
@ContextConfiguration(locations={"bean.xml"})
public class TestService {
		
	ContactServiceImpl service;;
	ApplicationContext context;
	File file;
	
	
	@Before
    public void initTest() {
		context = new ClassPathXmlApplicationContext("bean.xml");
		service = context.getBean("ContactService",ContactServiceImpl.class);
		file = new File("src/main/resources/ListContact");
    }
	
	@Test
	public void FileTest() {
		assertTrue(file.exists());
	}
	
	
	@Test
	public void testFindAll() {
		 Collection<Person> listePerson = service.listePerson(file);
	     assertNotNull("null !", listePerson);
	     assertTrue("Rien trouvé",listePerson.size() > 0);
	   }
	
	@Test
	public void testInsert() {
		Person p1 = new Person(7L,"Jhon","Doe",4920646400L);
		service.createPerson(file, p1);
	    assertFalse(service.getPerson(file, 7L).equals(null));
	}
	
	@Test
	public void testUpdate() throws IOException {
		Person p2 = new Person(7L,"Max","Maxi",4728928430L);
		service.updatePerson(file,p2);
		assertTrue(service.getPerson(file, 7L).getFirstName().equals("Max"));
		assertTrue(service.getPerson(file, 7L).getLastName().equals("Maxi"));
	}
	/*
	@Test
	public void testDelete() throws IOException {
		service.deletePerson(file, 7L);
		assertTrue(service.getPerson(file, 7L).equals(null));
	}
	*/


}